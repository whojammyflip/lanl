# https://www.kaggle.com/allunia/shaking-earth
# load packages
import numpy as np # linear algebra
import pandas as pd # data processing, CSV file I/O (e.g. pd.read_csv)
import matplotlib.pyplot as plt
# %matplotlib inline
import seaborn as sns
sns.set()

# from IPython.display import HTML
from os import listdir
#PATH = "/media/james/0640460B404601C3/Kaggle/LANL/input/"
# print(listdir("/media/james/samsung m850/kaggle/LANL/input/"))
print(listdir("/media/james/0640460B404601C3/Kaggle/LANL/input/"))

import warnings
warnings.filterwarnings("ignore", category=DeprecationWarning)
warnings.filterwarnings("ignore", category=UserWarning)
warnings.filterwarnings("ignore", category=FutureWarning)

# load training data
train = pd.read_csv("/media/james/0640460B404601C3/Kaggle/LANL/input/train.csv",
	dtype={'acoustic_data': np.int16, 'time_to_failure': np.float64})
print(train.head(5))

# js edit: count of earthquakes
diff_vector = np.diff(train['time_to_failure'].values)
diff_vector[diff_vector > 0] = 1
diff_vector[diff_vector <= 0] = 0
count_jumps = np.sum(diff_vector)
print(count_jumps)

# rename fields
train.rename({"acoustic_data": "signal", "time_to_failure": "quaketime"}, axis="columns", inplace=True)
print(train.head(5))
# pandas rounding shown up by extracting values and looking directly
for n in range(5):
    print(train.quaketime.values[n])

# EDA
fig, ax = plt.subplots(2,1, figsize=(20,12))
ax[0].plot(train.index.values[:100000000], train.quaketime.values[:100000000], c="darkred")
ax[0].set_title("Quaketime of 10 Mio rows")
ax[0].set_xlabel("Index")
ax[0].set_ylabel("Quaketime in ms");
ax[1].plot(train.index.values[:100000000], train.signal.values[:100000000], c="mediumseagreen")
ax[1].set_title("Signal of 10 Mio rows")
ax[1].set_xlabel("Index")
ax[1].set_ylabel("Acoustic Signal");
fig.show()

# EDA timesteps
fig, ax = plt.subplots(3,1,figsize=(20,18))
ax[0].plot(train.index.values[0:50000], train.quaketime.values[0:50000], c="Red")
ax[0].set_xlabel("Index")
ax[0].set_ylabel("Time to quake")
ax[0].set_title("How does the second quaketime pattern look like?")
ax[1].plot(train.index.values[0:49999], np.diff(train.quaketime.values[0:50000]))
ax[1].set_xlabel("Index")
ax[1].set_ylabel("Difference between quaketimes")
ax[1].set_title("Are the jumps always the same?")
ax[2].plot(train.index.values[0:4000], train.quaketime.values[0:4000])
ax[2].set_xlabel("Index from 0 to 4000")
ax[2].set_ylabel("Quaketime")
ax[2].set_title("How does the quaketime changes within the first block?");
fig.show()

# js, aggregate by value of time jump
diff_vector = np.diff(train['quaketime'].values)
diff_vector[diff_vector > -1e-8] = 0
counts_unique = np.unique(diff_vector, return_counts = True)
print(counts_unique)

# js: look at the values where the jumps in time are less than -1e-4
# EDA
diff_vector = np.append(np.diff(train['quaketime'].values),0)
idx = diff_vector < -1e-4
fig, ax = plt.subplots(2,1, figsize=(20,12))
ax[0].plot(train.index.values[idx], train.quaketime.values[idx], c="darkred")
ax[0].set_title("Quaketime of selected jump rows")
ax[0].set_xlabel("Index")
ax[0].set_ylabel("Quaketime in ms");
ax[1].plot(train.index.values[idx], train.signal.values[idx], c="mediumseagreen")
ax[1].set_title("Signal of selected jump rows")
ax[1].set_xlabel("Index")
ax[1].set_ylabel("Acoustic Signal");
fig.show()

# js: look at random values
# EDA
idx = (np.random.random(train.shape[0]) < 1/10000) & (np.abs(train.signal.values) < 200)
fig, ax = plt.subplots(2,1, figsize=(20,12))
ax[0].plot(train.index.values[idx], train.quaketime.values[idx], c="darkred")
ax[0].set_title("Quaketime of 1 in 10000 random rows")
ax[0].set_xlabel("Index")
ax[0].set_ylabel("Quaketime in ms");
ax[1].plot(train.index.values[idx], train.signal.values[idx], c="mediumseagreen")
ax[1].set_title("Signal of 1 in 10000 random rows")
ax[1].set_xlabel("Index")
ax[1].set_ylabel("Acoustic Signal");
fig.show()

# js: look at random segment same length as test data
# EDA
length_data = 15000
start_index = np.random.random_integers(0,len(train.index.values) - length_data,1)[0]
fig, ax = plt.subplots(2,1, figsize=(20,12))
idx = range(start_index,(start_index+length_data))
ax[0].plot(train.index.values[idx], train.quaketime.values[idx], c="darkred")
ax[0].set_title("Quaketime of random selection of 150000 rows")
ax[0].set_xlabel("Index")
ax[0].set_ylabel("Quaketime in ms");
ax[1].plot(train.index.values[idx], train.signal.values[idx], c="mediumseagreen")
ax[1].set_title("Signal of random selection of 150000 rows")
ax[1].set_xlabel("Index")
ax[1].set_ylabel("Acoustic Signal");
fig.show()
print(np.mean(train.signal.values[idx]))
print(np.std(train.signal.values[idx]))

# TEST DATA
test_path = "/media/james/0640460B404601C3/Kaggle/LANL/input/test/"
test_files = listdir("/media/james/0640460B404601C3/Kaggle/LANL/input/test")
print(test_files[0:5])
print(len(test_files))
sample_submission = pd.read_csv("/media/james/0640460B404601C3/Kaggle/LANL/input/sample_submission.csv")
print(sample_submission.head(2))
print(len(sample_submission.seg_id.values))

# look at the signal of the test data
length_data = 15000
start_index = np.random.random_integers(0,150000 - length_data,1)[0]
fig, ax = plt.subplots(2,1, figsize=(20,12))
idx = range(start_index,(start_index+length_data))

fig, ax = plt.subplots(4,1, figsize=(20,25))

for n in range(4):
    seg = pd.read_csv(test_path  + test_files[n])
    ax[n].plot(seg.acoustic_data.values[idx], c="mediumseagreen")
    ax[n].set_xlabel("Index")
    ax[n].set_ylabel("Signal")
    ax[n].set_title("Test {}".format(test_files[n]));
fig.show()

# train explorations
# train.describe() takes a long time to run

# train signal distribution
fig, ax = plt.subplots(1,2, figsize=(20,5))
sns.distplot(train.signal.values, ax=ax[0], color="Red", bins=100, kde=False)
ax[0].set_xlabel("Signal")
ax[0].set_ylabel("Density")
ax[0].set_title("Signal distribution")

low = train.signal.mean() - 10 * train.signal.std()
high = train.signal.mean() + 10 * train.signal.std() 
sns.distplot(train.loc[(train.signal >= low) & (train.signal <= high), "signal"].values,
             ax=ax[1],
             color="Orange",
             bins=150, kde=False)
ax[1].set_xlabel("Signal")
ax[1].set_ylabel("Density")
ax[1].set_title("Signal distribution without peaks")
fig.show()

